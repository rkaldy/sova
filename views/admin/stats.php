<h2>Statistiky</h2>

<ul>
  <li><a href="stat?type=rank">Pořadí týmů</a></li>
  <li><a href="stat?type=ciphers">Statistiky šifer a aktivit</a></li>
  <li><a href="stat?type=fastestsolved">Nejrychlejší týmy</a></li>
  <li><a href="stat?type=ccodes">Céčka</a></li>
  <li><a href="stat?type=barchart">Barchart race</a> (zdrojové CSV)</li>
</ul>
