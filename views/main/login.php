<h2>Přihlášení týmu</h2>

<?php 
if (isset($flash)) echo "<p id=\"flash\">$flash</p>";
?>

<p>Zadejte přihlašovací údaje vašeho týmu:</p>

<form method="post" action="login">
  <table>
    <tr>
      <th><label for="team_id">číslo týmu</label></th>
      <td><input type="text" name="team_id" class="focused" size="20" maxlength="50"></td>
    </tr>
    <tr>
      <th><label for="pswd">heslo</label></th>
	  <td><input type="password" id="pswd" name="pswd" size="20" maxlength="50"></td>
    </tr>
    <tr>
      <th></th>
	  <td><input type="submit" value="Přihlásit"></td>
    </tr>
  </table>
</form>
