code.unknown;Neznámý kód: %s
code.invalid;Kód musí být jednoslovné podstatné jméno.
loc.visited;Vítejte na stanovišti %s. Máte %s bodů.
loc.finish.visited;Gratulujeme, jste v cíli! Celkem jste dosáhli %s bodů.
loc.rank;Jste tu %s. První tu byl tým %s v %s.
loc.already;Tento kód stanoviště jste již zadali.
loc.next;Další stanoviště %s se nachází %s.
cipher.unknown;Neznámé číslo šifry či aktivity: %s
cipher.no-previous-cipher;Na šifru %s jste se ještě nemohli dostat, protože jste nevyluštili žádnou předchozí šifru.
cipher.no-previous-loc;Na šifru %s jste se ještě nemohli dostat, protože jste nezadali kód jejího stanoviště.
cipher.solved;Úspěšně jste vyluštili šifru %s. Máte %s bodů.
cipher.rank;Jste %s. První ji vyluštil tým %s v %s.
cipher.already;Toto řešení šifry jste již zadali.
cipher.hint;Nápověda k šifře %s: %s.
cipher.howto;Postup k šifře %s: %s
cipher.solution;Řešení šifry %s: %s
activity.solved;Úspěšně jste zvládli aktivitu %s. Máte %s bodů.
activity.already;Tento kód aktivity jste již zadali.
activity.rank;Jste %s. První ji dal tým %s v %s.
hint.check;(Kontrola nápovědy na %s)
hint.request;(Žádost o nápovědu na %s)
hint.apply.activity;K aktivitám žádné nápovědy nejsou.
hint.apply.no-hint;K této šifře žádná nápověda není.
hint.apply.solved;Šifru %s jste již vyluštili.
hint.apply.already;Pro šifru %s jste již dostali řešení. Podívejte se do přehledu šifer.
hint.apply.history;Pro šifru %s jste již dostali %s.
hint.apply.ccodes;Nyní můžete zažádat o %s za %s céček.
hint.apply.points;Nyní můžete zažádat o %s za %s bodů.
hint.apply.points.no-ccode;Nyní můžete zažádat o %s. Protože nemáte dostatek céček, bude vás to stát %s bodů.
hint.text.hint;Nápověda k šifře %s: %s
hint.text.howto;Postup k šifře %s: %s
hint.text.solution;Řešení šifry %s: %s
ccode.add.already;Toto céčko jste již zadali.
ccode.add.success;Získali jste céčko. Aktuálně máte %s nevyužitých céček.
imunity.available;Můžete si zakoupit imunitu za %s céček.
imunity.already;Imunitu jste již dostali.
imunity.insufficient;Nemáte dostatek céček pro získání imunity.
imunity.success;Získali jste imunitu. Kdyby na vás při vyhlášení padlo organizování příštího ročníku, můžete ho odmítnout.
hint.apply.no-history;Pro šifru %s jste ještě žádnou nápovědu nedostali.
